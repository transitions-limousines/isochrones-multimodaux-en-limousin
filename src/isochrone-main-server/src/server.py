import os
from typing import List, Optional

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import requests
import uvicorn

from model import IsochroneRequest, IsochroneResponse
from model import ModeComparisonRequest, ModeComparisonResponse
from model import CoverageRequest, CoverageResponse
from model import TimeComparisonRequest, TimeComparisonResponse
from model import PopulationRequest, PopulationResponse
from model import Isochrone


THIS_DIR: str = os.path.dirname(os.path.abspath(__file__))

FASTAPI_HOST = "0.0.0.0"
FASTAPI_PORT = 9000

OTP_CLIENT_URL = f"http://{os.environ.get('SERVICES_HOST')}:9001"
POPULATION_SERVICE_URL = f"http://{os.environ.get('SERVICES_HOST')}:9002/compute_population"


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "PATCH"],
    allow_headers=["*"],
)


def mode_comparison(request: ModeComparisonRequest) -> Optional[ModeComparisonResponse]:
    # send request to OTP
    response = request_otp(request)
    if response is None:
        print(f"Request failed: {request}")
        print(f"Check OTP client log")
    if request.compute_population:
        population = compute_population(response.isochrones)
        if population is not None:
            response.isochrones = population.isochrones
        else:
            print(f"Population request failed: {request}")
            print(f"Check population service log")
    return response


def coverage(request: CoverageRequest) -> Optional[CoverageResponse]:
    # send request to OTP
    response = request_otp(request)
    if response is None:
        print(f"Request failed: {request}")
        print(f"Check OTP client log")
    if request.compute_population:
        population = compute_population(response.isochrones)
        if population is not None:
            response.isochrones = population.isochrones
        else:
            print(f"Population request failed: {request}")
            print(f"Check population service log")
    if not response.isochrones:
        # print error and abort
        return
    return response


def time_comparison(request: TimeComparisonRequest) -> Optional[TimeComparisonResponse]:
    # send request to OTP
    response = request_otp(request)
    if response is None:
        print(f"Request failed: {request}")
        print(f"Check OTP client log")
    if request.compute_population:
        population = compute_population(response.isochrones)
        if population is not None:
            response.isochrones = population.isochrones
        else:
            print(f"Population request failed: {request}")
            print(f"Check population service log")
    if not response.isochrones:
        # print error and abort
        return
    return response


def request_otp(request: IsochroneRequest) -> Optional[IsochroneResponse]:
    endpoint, response_cls = "", None
    if isinstance(request, ModeComparisonRequest):
        endpoint, response_cls = "mode_comparison", ModeComparisonResponse
    if isinstance(request, CoverageRequest):
        endpoint, response_cls = "coverage", CoverageResponse
    if isinstance(request, TimeComparisonRequest):
        endpoint, response_cls = "time_comparison", TimeComparisonResponse
    resp = requests.post(f"{OTP_CLIENT_URL}/{endpoint}", json=request.model_dump(), timeout=30)
    if resp.status_code == 200:
        return response_cls(**resp.json())
    print(resp.status_code)
    print(resp.content)


def compute_population(isochrones: List[Isochrone]) -> Optional[PopulationResponse]:
    request = PopulationRequest(isochrones=isochrones)
    resp = requests.post(POPULATION_SERVICE_URL, json=request.model_dump())
    if resp.status_code == 200:
        return PopulationResponse(**resp.json())
    print(resp.status_code)
    print(resp.content)


@app.get("/mode_comparison", response_model=ModeComparisonResponse)
async def process_mode_comparison_request(request: ModeComparisonRequest):
    return mode_comparison(request)


@app.get("/coverage", response_model=CoverageResponse)
async def process_coverage_request(request: CoverageRequest):
    return coverage(request)


@app.get("/time_comparison", response_model=TimeComparisonResponse)
async def process_mode_comparison_request(request: TimeComparisonRequest):
    return time_comparison(request)


def run_api() -> None:
    # atexit.register(kill_all_subprocesses)
    uvicorn.run(app, host=FASTAPI_HOST, port=FASTAPI_PORT, log_config=os.path.join(THIS_DIR, "log.ini"))


if __name__ == '__main__':

    run_api()
