import geopandas as gpd

from db import load_db
from model import Isochrone

print("read population database")
population_gdf = load_db()
print("done with population database")


def populate_isochrone(isochrone: Isochrone) -> Isochrone:
    gdf = isochrone.geodataframe
    # convert to cartesian coordinates (EPSG:3857)
    # https://gis.stackexchange.com/a/218453
    gdf = gdf.to_crs(population_gdf.crs)
    overlay = gpd.overlay(gdf, population_gdf, how="intersection").drop(columns=["P20_POP"])
    overlayed_iris = population_gdf.loc[population_gdf.CODE_IRIS.isin(overlay.CODE_IRIS), :]
    overlay["overlay_area"] = overlay["geometry"].area
    overlayed_iris.insert(0, "iris_area", overlayed_iris["geometry"].area)
    overlayed_iris = overlayed_iris.drop(columns=["geometry"])
    overlay = overlay.merge(overlayed_iris, left_on="CODE_IRIS", right_on="CODE_IRIS")
    overlay.insert(0, "ratio", overlay["overlay_area"] / overlay["iris_area"])
    overlay.insert(0, "population_in_overlay", (overlay["P20_POP"] * overlay["ratio"]).astype(int))
    isochrone.population = overlay["population_in_overlay"].sum().item()  # item() converts np.int64 to python int
    return isochrone
