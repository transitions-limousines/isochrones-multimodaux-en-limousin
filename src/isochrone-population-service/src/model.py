import tempfile
import traceback
from typing import List, Optional, Union
import uuid

import geopandas as gpd
from pydantic import BaseModel as PydanticBaseModel
from pydantic import Field, field_serializer, field_validator


class BaseModel(PydanticBaseModel):
    class Config:
        arbitrary_types_allowed = True


class Isochrone(BaseModel):
    geodataframe: gpd.GeoDataFrame
    travel_mode: str
    time: str
    population: Optional[int] = None

    @field_validator("geodataframe", mode="before")
    @classmethod
    def load_gdf_from_geojson(cls, raw: Union[gpd.GeoDataFrame, str]) -> gpd.GeoDataFrame:
        if isinstance(raw, gpd.GeoDataFrame):
            return raw
        if isinstance(raw, str):
            with tempfile.TemporaryFile() as fp:
                fp.write(raw.encode())
                fp.seek(0)
                try:
                    return gpd.read_file(fp)
                except Exception as e:
                    print(traceback.format_exc())
        return gpd.GeoDataFrame()

    @field_serializer('geodataframe')
    def serialize_geodataframe_to_geojson(self, gdf: gpd.GeoDataFrame, _info):
        return gdf.to_json()


class PublicTransportationOptions(BaseModel):
    hour: str  # HH:MM
    use_departure_time: bool  # true: use departure time, false: use arrival time
    regime: str  # Q: everyday (quotidien), SLF: saturday/day after a celebration (samedi/lendemain de fête), DF: sunday/public holiday (Dimanche/Férié)


class IsochroneRequest(BaseModel):
    id: str = Field(default_factory=lambda: uuid.uuid4().hex)
    max_travel_duration_minutes: int
    public_transportation: Optional[PublicTransportationOptions] = None
    compute_population: bool  # cross-match with population database (INSEE IRIS)


class IsochroneResponse(BaseModel):
    isochrones: List[Isochrone] = Field(default_factory=list)


class PopulationRequest(BaseModel):
    id: str = Field(default_factory=lambda: uuid.uuid4().hex)
    isochrones: List[Isochrone] = Field(default_factory=list)


class PopulationResponse(BaseModel):
    request_id: str
    isochrones: List[Isochrone] = Field(default_factory=list)


class Point(BaseModel):
    latitude: float  # WGS84
    longitude: float  # WGS84


class ModeComparisonRequest(IsochroneRequest):
    point: Point
    travel_modes: List[str]


class ModeComparisonResponse(IsochroneResponse):
    request: ModeComparisonRequest


class CoverageRequest(IsochroneRequest):
    points: List[Point]
    travel_mode: str


class CoverageResponse(IsochroneResponse):
    request: CoverageRequest


class TimeComparisonRequest(IsochroneRequest):
    point: Point
    travel_mode: str
    start_hour: str
    end_hour: str
    n_iterations: int


class TimeComparisonResponse(IsochroneResponse):
    request: TimeComparisonRequest
