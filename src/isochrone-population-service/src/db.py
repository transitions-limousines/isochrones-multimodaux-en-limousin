# -*- coding: utf-8 -*-
"""
Fetches required population-related databases from established sources (e.g. INSEE, IGN).
"""

import os
import shutil
import zipfile
import sys
import tempfile
from typing import Optional

import requests
import py7zr
import pandas as pd
import sqlite3
import geopandas as gpd
from shapely import wkt


THIS_DIR: str = os.path.dirname(os.path.abspath(__file__))
DATA_DIR: str = os.path.join(THIS_DIR, "data")

os.makedirs(DATA_DIR, exist_ok=True)

# Below are the public URLs for building the population database
# These URLs may change over time, as well as the file structure inside each archive
# You may have to make some manual changes to generate your own database

# This URL may change
POPULATION_FILE_URL = "https://www.insee.fr/fr/statistiques/fichier/7704076/base-ic-evol-struct-pop-2020_xlsx.zip"
POPULATION_FILENAME_ZIPPED = "base-ic-evol-struct-pop-2020_xlsx.zip"
POPULATION_FILENAME_UZ = "base-ic-evol-struct-pop-2020.xlsx"
POPULATION_FILE = os.path.join(DATA_DIR, POPULATION_FILENAME_UZ)

# This URL may change
CONTOURS_FILE_URL = "https://data.geopf.fr/telechargement/download/CONTOURS-IRIS/CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01/CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01.7z"
CONTOURS_FILENAME_ZIPPED = "CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01.7z"
CONTOURS_FILENAME_UZ = "CONTOURS-IRIS_3-0__SHP__FRA_2023-01-01"
CONTOURS_DIR = os.path.join(DATA_DIR, CONTOURS_FILENAME_UZ)
# The path to CONTOURS-IRIS.shp may be different
# Check the archive file manually and adapt the file path if needed
CONTOURS_FILE = os.path.join(
    CONTOURS_DIR,
    "CONTOURS-IRIS/1_DONNEES_LIVRAISON_2024-02-00238/CONTOURS-IRIS_3-0_SHP_LAMB93_FXX-ED2023-01-01/CONTOURS-IRIS.shp",
)

# This is the final DB. It is a single-file database with 3 columns : IRIS, GEOMETRY, POPULATION
# File size is approx. 350MB
POPULATION_DB = os.path.join(DATA_DIR, "population_by_iris.sqlite")

PROJECTION = "epsg:3857"  # cartesian system


def geodataframe_from_json(json_string: str) -> gpd.GeoDataFrame:
    with tempfile.TemporaryFile() as fp:
        fp.write(json_string.encode())
        fp.seek(0)
        return gpd.read_file(fp)


def geodataframe_to_json(gdf: gpd.GeoDataFrame) -> str:
    return gdf.to_json()


def load_db() -> gpd.GeoDataFrame:
    gdf = get_db()
    if gdf is not None:
        return gdf
    with sqlite3.connect(POPULATION_DB) as conn:
        gdf = pd.read_sql("select * from population", conn)
        gdf['geometry'] = gdf.geometry.apply(wkt.loads)
        return gpd.GeoDataFrame(gdf, crs=PROJECTION, geometry=gdf.pop("geometry"))


def get_db() -> Optional[gpd.GeoDataFrame]:
    if not os.path.isfile(POPULATION_DB):
        gdf = load_contour_db().merge(get_population_db(), left_on="CODE_IRIS", right_on="IRIS").drop(columns=["IRIS"])
        print("Dumping db to sqlite...")
        gdf['geometry'] = gdf.geometry.apply(wkt.dumps)
        with sqlite3.connect(POPULATION_DB) as conn:
            gdf.to_sql("population", conn, if_exists='replace', index=False)
        gdf['geometry'] = gdf.geometry.apply(wkt.loads)
        return gpd.GeoDataFrame(gdf, crs=PROJECTION, geometry=gdf.pop("geometry"))


def get_population_db() -> Optional[pd.DataFrame]:
    get_population_file()
    print("Loading population file to pandas...")
    df = pd.read_excel(POPULATION_FILE, sheet_name="IRIS", skiprows=5)
    df = df[["IRIS", "P20_POP"]]
    print("Cleaning population file...")
    os.remove(POPULATION_FILE)
    return df


def get_population_file() -> None:
    if not os.path.exists(POPULATION_FILE):
        print("Downloading population file...")
        response = requests.get(POPULATION_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(POPULATION_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping population file...")
        with zipfile.ZipFile(POPULATION_FILENAME_ZIPPED, "r") as zipf:
            zipf.extractall(DATA_DIR)
        os.remove(POPULATION_FILENAME_ZIPPED)


def load_contour_db() -> Optional[gpd.GeoDataFrame]:
    get_contour_file()
    print("Loading contour file to pandas...")
    gdf = gpd.read_file(CONTOURS_FILE)[["CODE_IRIS", "geometry"]].to_crs(PROJECTION)
    print("Cleaning contour directory...")
    shutil.rmtree(CONTOURS_DIR)
    return gdf


def get_contour_file() -> None:
    if not os.path.exists(CONTOURS_FILE):
        print("Downloading contour file...")
        response = requests.get(CONTOURS_FILE_URL)
        if response.status_code != 200:
            print(response)
            print("Error")
            sys.exit(1)
        with open(CONTOURS_FILENAME_ZIPPED, "wb") as f:
            f.write(response.content)
        print("Unzipping contour file...")
        with py7zr.SevenZipFile(CONTOURS_FILENAME_ZIPPED, mode='r') as z:
            z.extractall(DATA_DIR)
        os.remove(CONTOURS_FILENAME_ZIPPED)
