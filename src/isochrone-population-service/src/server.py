import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from model import PopulationRequest, PopulationResponse
from population import populate_isochrone


THIS_DIR: str = os.path.dirname(os.path.abspath(__file__))

FASTAPI_HOST = "0.0.0.0"
FASTAPI_PORT = 9002


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "PATCH"],
    allow_headers=["*"],
)


def compute_population(request: PopulationRequest) -> PopulationResponse:
    response = PopulationResponse(request_id=request.id)
    for isochrone in request.isochrones:
        isochrone = populate_isochrone(isochrone)
        response.isochrones.append(isochrone)
    return response


@app.post("/compute_population", response_model=PopulationResponse)
async def process_compute_population_request(request: PopulationRequest):
    return compute_population(request)


def run_api() -> None:
    uvicorn.run(app, host=FASTAPI_HOST, port=FASTAPI_PORT, log_config=os.path.join(THIS_DIR, "log.ini"))


if __name__ == '__main__':

    run_api()
