from datetime import datetime, time, timedelta
import json
import os
import tempfile
from typing import List, Optional

import geopandas as gpd
import holidays
import pandas as pd
import pytz
import requests
import tzlocal

from model import Isochrone, PublicTransportationOptions
from model import ModeComparisonRequest, CoverageRequest, TimeComparisonRequest


OTP_SERVER_URL = os.environ.get("OTP_SERVER_URL")

# read local timezone from host
LOCAL_TIMEZONE = pytz.timezone(tzlocal.get_localzone_name())


def localize_datetime(d: datetime) -> datetime:
    return d.replace(tzinfo=LOCAL_TIMEZONE)


def geodataframe_from_json(json_string: str) -> gpd.GeoDataFrame:
    with tempfile.TemporaryFile() as fp:
        fp.write(json_string.encode())
        fp.seek(0)
        return gpd.read_file(fp)


def geodataframe_to_json(gdf: gpd.GeoDataFrame) -> str:
    return gdf.to_json()


def generate_time_credit_string(date_start: datetime, max_travel_duration_s: float) -> str:
    dt = timedelta(seconds=max_travel_duration_s)
    d1 = date_start + dt
    h = 24 * (d1.day - date_start.day)  # cannot go further in the future than 1 month
    m = d1.minute
    s = d1.second
    return f"{h}H{m}M{s}S"


def request_isochrone(longitude_deg: float,
                      latitude_deg: float,
                      date_start: datetime,
                      use_departure_time: bool,
                      max_travel_duration_s: float,
                      travel_mode: str) -> Optional[Isochrone]:
    time_credit_str = generate_time_credit_string(date_start, max_travel_duration_s)
    params = {'batch': True,
              "arriveBy": use_departure_time,
              'location': f'{latitude_deg},{longitude_deg}',
              "time": date_start.isoformat(timespec="seconds"),
              "modes": travel_mode,
              "cutoff": time_credit_str,
              "walkSpeed": 1.11,  # 4 km/h
              "bikeSpeed": 2.78,  # 10 km/h
              "carSpeed": 8.33}  # 30 km/h
    response = requests.get(OTP_SERVER_URL, params=params)
    if response.status_code != 200:
        print(f"Failed to request isochrone {params} : {response.status_code}")
        print(response.content)
        return None
    # The geometry returned by OTP is in lon/lat (EPSG:4326) format
    gdf = geodataframe_from_json(json.dumps(response.json())).dissolve()
    # The id column is an internal thing to OTP, we don't need it
    # We don't know what the time column relates to, so we drop it
    gdf.drop(columns=["id", "time"], inplace=True)
    return Isochrone(geodataframe=gdf, travel_mode=travel_mode, time=date_start.isoformat(timespec="seconds"))


def find_next_ordinary_day() -> datetime.date:
    now = datetime.now()
    supplementary_days = 0
    holidays_in_france = holidays.FR()
    while (now + timedelta(days=supplementary_days)).weekday() > 4 or now + timedelta(days=supplementary_days) in holidays_in_france:
        supplementary_days += 1
    return (now + timedelta(days=supplementary_days)).date()


def find_next_saturday() -> datetime.date:
    now = datetime.now()
    supplementary_days = 0
    while (now + timedelta(days=supplementary_days)).weekday() != 5:
        supplementary_days += 1
    return (now + timedelta(days=supplementary_days)).date()


def find_next_sunday() -> datetime.date:
    now = datetime.now()
    supplementary_days = 0
    while (now + timedelta(days=supplementary_days)).weekday() != 6:
        supplementary_days += 1
    return (now + timedelta(days=supplementary_days)).date()


def compute_date_start(public_transportation: Optional[PublicTransportationOptions]) -> datetime:
    if public_transportation is None:
        return localize_datetime(datetime.now())
    day = datetime.now().date()
    match public_transportation.regime:
        case "Q":
            day = find_next_ordinary_day()
        case "SLF":
            day = find_next_saturday()
        case "DF":
            day = find_next_sunday()
    t = datetime.strptime(public_transportation.hour, "%HH%M")
    return localize_datetime(datetime.combine(day, time(hour=t.hour, minute=t.minute)))


def request_mode_comparison_isochrones(request: ModeComparisonRequest) -> List[Isochrone]:
    result = []
    date_start = compute_date_start(request.public_transportation)
    use_departure_time = True
    if request.public_transportation is not None:
        use_departure_time = request.public_transportation.use_departure_time
    for travel_mode in request.travel_modes:
        isochrone = request_isochrone(longitude_deg=request.point.longitude,
                                      latitude_deg=request.point.latitude,
                                      date_start=date_start,
                                      use_departure_time=use_departure_time,
                                      max_travel_duration_s=request.max_travel_duration_minutes * 60.,
                                      travel_mode=travel_mode)
        if isochrone is not None:
            result.append(isochrone)
    return result


def request_coverage_isochrone(request: CoverageRequest) -> List[Isochrone]:
    result = []
    date_start = compute_date_start(request.public_transportation)
    use_departure_time = True
    if request.public_transportation is not None:
        use_departure_time = request.public_transportation.use_departure_time
    for point in request.points:
        isochrone = request_isochrone(longitude_deg=point.longitude,
                                      latitude_deg=point.latitude,
                                      date_start=date_start,
                                      use_departure_time=use_departure_time,
                                      max_travel_duration_s=request.max_travel_duration_minutes * 60.,
                                      travel_mode=request.travel_mode)
        if isochrone is not None:
            result.append(isochrone)
    merged_gdf = gpd.GeoDataFrame(pd.concat(map(lambda isochrone: isochrone.geodataframe, result))).dissolve()
    return [Isochrone(geodataframe=merged_gdf, travel_mode=request.travel_mode, time=date_start.isoformat(timespec="seconds"))]


def compute_dates_in_interval(start_date: datetime, end_date: datetime, n_iterations: int) -> List[datetime]:
    interval = (end_date - start_date) / n_iterations
    return [start_date + i*interval for i in range(n_iterations+1)]  # include end_date in the list


def compute_date_start_array(start_hour: str,
                             end_hour: str,
                             n_iterations: int,
                             public_transportation: Optional[PublicTransportationOptions]) -> List[datetime]:
    # FIXME : In this implementation, start_hour and end_hour take precedence over public_transportation.hour (which is ignored)
    start_hour = datetime.strptime(start_hour, "%HH%M")
    end_hour = datetime.strptime(end_hour, "%HH%M")
    day = datetime.now()
    if public_transportation is not None:
        day = datetime.now().date()
        match public_transportation.regime:
            case "Q":
                day = find_next_ordinary_day()
            case "SLF":
                day = find_next_saturday()
            case "DF":
                day = find_next_sunday()
    start_date = localize_datetime(datetime.combine(day, datetime.min.time()).replace(hour=start_hour.hour,
                                                                                      minute=start_hour.minute))
    end_date = localize_datetime(datetime.combine(day, datetime.min.time()).replace(hour=end_hour.hour,
                                                                                    minute=end_hour.minute))
    return compute_dates_in_interval(start_date, end_date, n_iterations)


def request_time_comparison_isochrones(request: TimeComparisonRequest) -> List[Isochrone]:
    result = []
    date_start_array = compute_date_start_array(request.start_hour,
                                                request.end_hour,
                                                request.n_iterations,
                                                request.public_transportation)
    use_departure_time = True
    if request.public_transportation is not None:
        use_departure_time = request.public_transportation.use_departure_time
    for date_start in date_start_array:
        isochrone = request_isochrone(longitude_deg=request.point.longitude,
                                      latitude_deg=request.point.latitude,
                                      date_start=date_start,
                                      use_departure_time=use_departure_time,
                                      max_travel_duration_s=request.max_travel_duration_minutes * 60.,
                                      travel_mode=request.travel_mode)
        if isochrone is not None:
            result.append(isochrone)
    return result
