import os

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import uvicorn

from model import CoverageRequest, CoverageResponse
from model import ModeComparisonRequest, ModeComparisonResponse
from model import TimeComparisonRequest, TimeComparisonResponse
import otp

THIS_DIR: str = os.path.dirname(os.path.abspath(__file__))

FASTAPI_HOST = "0.0.0.0"
FASTAPI_PORT = 9001


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE", "PATCH"],
    allow_headers=["*"],
)


def mode_comparison(request: ModeComparisonRequest) -> ModeComparisonResponse:
    response = ModeComparisonResponse(request=request,
                                      isochrones=otp.request_mode_comparison_isochrones(request))
    return response


@app.post("/mode_comparison", response_model=ModeComparisonResponse)
async def process_mode_comparison_request(request: ModeComparisonRequest):
    return mode_comparison(request)


def coverage(request: CoverageRequest) -> CoverageResponse:
    response = CoverageResponse(request=request,
                                isochrones=otp.request_coverage_isochrone(request))
    return response


@app.post("/coverage", response_model=CoverageResponse)
async def process_coverage_request(request: CoverageRequest):
    return coverage(request)


def time_comparison(request: TimeComparisonRequest) -> TimeComparisonResponse:
    response = TimeComparisonResponse(request=request,
                                      isochrones=otp.request_time_comparison_isochrones(request))
    return response


@app.post("/time_comparison", response_model=TimeComparisonResponse)
async def process_time_comparison_request(request: TimeComparisonRequest):
    return time_comparison(request)


def run_api() -> None:
    uvicorn.run(app, host=FASTAPI_HOST, port=FASTAPI_PORT, log_config=os.path.join(THIS_DIR, "log.ini"))


if __name__ == '__main__':

    run_api()
