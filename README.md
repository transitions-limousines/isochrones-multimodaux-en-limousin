# Isochrones multimodaux en Limousin

Cette API permet de requêter des isochrones auprès d'un serveur OpenTripPlanner (OTP) et de les croiser avec la base de données de population de l'INSEE.

## Description

L'API est écrite en python et utilise FastAPI. Elle est déployée avec docker. Il y a trois services distincts : 

- main-server (port 9000) : reçoit la requête d'un client, utilise les services de génération d'isochrone et de croisement avec la base INSEE pour générer une réponse et la retourne au client
- otp-client (port 9001) : reçoit une reqûete de génération d'isochrone par le main-server, crée la requête OTP correspondante et la transmet au serveur OTP externe. La réponse du serveur OTP est formatée pour que le main-server puisse la traiter
- population-service (port 9002) : construit la base de données de population au démarrage, puis traite les requêtes envoyées par le main-server

## Installation

Install docker :

```
sudo bash src/install-docker.sh
```

Then build and start the containers : 

```
docker compose -f ./src/docker-compose.yml up -d --build
```

When building is done, the main-server is ready to process request on port 9000.

When running on a host, setting up a DynDNS may be useful in order to access the service remotely. For example, see https://lucasvidelaine.wordpress.com/2020/11/15/dyndns-ovh/  


## Example usage

There are three types of requests available :

- Mode comparison : get one isochrone per transporation mode (walk, run, bike, public transportation, ...) on a single location
- Coverage : get one isochrone for a given transporation mode, on multiple locations
- Time comparison : get one isochrone per date, for a given transporation mode, on a single location

### Mode comparison 

```
curl -i -X GET http://localhost:9000/mode_comparison -H "Content-Type: application/json" -d '{"point": {"latitude": 45.83203, "longitude": 1.25714}, "max_travel_duration_minutes": 15, "compute_population": true, "travel_modes": ["CAR,TRANSIT", "BICYCLE,TRANSIT", "CAR", "TRANSIT", "BICYCLE", "WALK"], "public_transportation": {"hour": "13H52", "use_departure_time": false, "regime": "Q"}}' 
```

- `travel_modes` : list of travel modes supported by OTP, see https://docs.opentripplanner.org/en/v2.3.0/RoutingModes/
- `max_travel_duration_minutes` : The maximum duration of the entire trip, in minutes
- `use_departure_time` : For public transportation, interpret the provided `hour` parameter as a departure time (if true), or an arrival time (if false)  
- `regime`: For public transportation, can be either "Q" (Quotidien) for weekdays, "SLF" for saturdays and public holidays aftermath (Samedi et lendemain de fête), "DF" for sundays and public holidays (Dimanche et Férié)
- `compute_population`: if true, cross-match the isochrone with the INSEE database to compute an estimate of the population living within the area covered by the isochrone

Request schema: 

```
{
  "point": {
    "latitude": float,
    "longitude": float
  },
  "travel_modes": str,
  "max_travel_duration_minutes": int,
  "public_transportation": {
    "hour": str,
    "use_departure_time": bool,
    "regime": str
  },
  "compute_population": bool
}
```

Response schema:

```
{
  "request":
  {
    "id": str,
    "point": {
      "latitude": float,
      "longitude": float
    },
    "travel_modes": str,
    "max_travel_duration_minutes": int,
    "public_transportation": {
      "hour": str,
      "use_departure_time": bool,
      "regime": str
    },
    "compute_population": bool
  },
  "isochrones": [
    {
      "geodataframe": {},
      "travel_mode": str,
      "time": str,
      "population": int | null
    }
  ]
}
```


### Coverage

```
curl -i -X GET http://localhost:9000/coverage -H "Content-Type: application/json" -d '{"points": [{"latitude": 45.83203, "longitude": 1.25714}, {"latitude": 45.90495, "longitude": 1.50357}], "max_travel_duration_minutes": 15, "compute_population": true, "travel_mode": "BICYCLE", "public_transportation" : {"hour": "13H52", "use_departure_time": false, "regime": "DF"}}' 
```

- `travel_mode` : A travel mode supported by OTP, see https://docs.opentripplanner.org/en/v2.3.0/RoutingModes/
- `max_travel_duration_minutes` : The maximum duration of the entire trip, in minutes
- `use_departure_time` : For public transportation, interpret the provided `hour` parameter as a departure time (if true), or an arrival time (if false)  
- `regime`: For public transportation, can be either "Q" (Quotidien) for weekdays, "SLF" for saturdays and public holidays aftermath (Samedi et lendemain de fête), "DF" for sundays and public holidays (Dimanche et Férié)
- `compute_population`: if true, cross-match the isochrone with the INSEE database to compute an estimate of the population living within the area covered by the isochrone

Request schema: 

```
{
  "points": [
    {
      "latitude": float,
      "longitude": float
    }
  ],
  "travel_mode": str,
  "max_travel_duration_minutes": int,
  "public_transportation": {
    "hour": str,
    "use_departure_time": bool,
    "regime": str
  },
  "compute_population": bool
}
```

Response schema:

```
{
  "request":
  {
    "id": str,
    "points": [
      {
        "latitude": float,
        "longitude": float
      }
    ],
    "travel_mode": str,
    "max_travel_duration_minutes": int,
    "public_transportation": {
      "hour": str,
      "use_departure_time": bool,
      "regime": str
    },
    "compute_population": bool
  },
  "isochrones": [
    {
      "geodataframe": {},
      "travel_mode": str,
      "time": str,
      "population": int | null
    }
  ]
}
```

### Time comparison 

```
curl -i -X GET http://localhost:9000/time_comparison -H "Content-Type: application/json" -d '{"point": {"latitude": 45.83203, "longitude": 1.25714}, "max_travel_duration_minutes": 15, "compute_population": true, "travel_mode": "BICYCLE", "start_hour": "03h00", "end_hour": "04h00", "n_iterations": 4, "public_transportation": {"hour": "13H52", "use_departure_time": false, "regime": "Q"}}' 
```

- `travel_mode` : A travel mode supported by OTP, see https://docs.opentripplanner.org/en/v2.3.0/RoutingModes/
- `start_hour` : The initial hour at which isochrones are computed
- `end_hour` : The final hour at which isochrones are computed
- `n_iterations` : number of isochrones computed in the interval \[start_hour, end_hour\]. For example, 3 intervals between 00h00 and 01h00 will generate 4 isochrones: 00h00, 00h20, 00h40 and 01h00.
- `max_travel_duration_minutes` : The maximum duration of the entire trip, in minutes
- `use_departure_time` : For public transportation, interpret the provided `hour` parameter as a departure time (if true), or an arrival time (if false)  
- `regime`: For public transportation, can be either "Q" (Quotidien) for weekdays, "SLF" for saturdays and public holidays aftermath (Samedi et lendemain de fête), "DF" for sundays and public holidays (Dimanche et Férié)
- `compute_population`: if true, cross-match the isochrone with the INSEE database to compute an estimate of the population living within the area covered by the isochrone


Request schema: 

```
{
  "point": {
    "latitude": float,
    "longitude": float
  },
  "travel_mode": str,
  "start_hour": str,
  "end_hour": str,
  "n_iterations": int,
  "max_travel_duration_minutes": int,
  "public_transportation": {
    "hour": str,
    "use_departure_time": bool,
    "regime": str
  },
  "compute_population": bool
}
```

Response schema:

```
{
  "request":
  {
    "id": str,
    "point": {
      "latitude": float,
      "longitude": float
    },
    "travel_mode": str,
    "start_hour": str,
    "end_hour": str,
    "n_iterations": int,
    "max_travel_duration_minutes": int,
    "public_transportation": {
      "hour": str,
      "use_departure_time": bool,
      "regime": str
    },
    "compute_population": bool
  },
  "isochrones": [
    {
      "geodataframe": {},
      "travel_mode": str,
      "time": str,
      "population": int | null
    }
  ]
}
```

